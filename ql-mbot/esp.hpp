#pragma once

#include "ql_render.hpp"

class ESP {
private:

    // draw name esp
	void draw_name(Clientinfo * ci, int x, int y, int w, int h);

    // draw 2d bounding box
    void draw_box(team_t team, bool visible, int x, int y, int w, int h);

    // draw weapon held esp
    void draw_weapon(Entity * ent, int x, int y, int w, int h);

    // construct 2d bounding box
	bool make_box(Entity * ent, Clientinfo * ci, int & x, int & y, int & w, int & h);

public:

    // ctor
	ESP();

    // draw / think
	void draw();
};

extern ESP g_esp;