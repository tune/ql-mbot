#pragma once

#include "ql_client.hpp"

class Aimbot {
private:

    // checks if the entity is valid.
    bool is_valid_target(Entity * ent, Clientinfo * info);

    // get entity hitbox.
    bool get_aimpoint(Entity *ent, Clientinfo* info, Vector3 &out_pos);

    // comp machinegun/chaingun spread (useless?)
    void no_spread();

    // get best aim angles.
    bool get_aim_angles(Vector3 &out_angles);

    // be a faggot
    void name_spam();

    // fix movement for silent aim
    void fix_move(Vector3 aim_angles);

    // steal names
    void name_steal();

    // get fov
    inline float get_fov(Vector3 va, Vector3 start, Vector3 end) {
        Vector3 ang{}, aim{}, dir = (end - start).normalized();

        auto vector_angles = [](Vector3 fwd, Vector3& angles) {
            if (fwd[1] == 0.0f && fwd[0] == 0.0f) {
                angles[0] = (fwd[2] > 0.0f) ? 270.0f : 90.0f;
                angles[1] = 0.0f;
            }
            else {
                float len2d = sqrt((fwd[0]* fwd[0]) + (fwd[1]* fwd[1]));

                angles[0] = Math::toDeg(atan2f(-fwd[2], len2d));
                angles[1] = Math::toDeg(atan2f(fwd[1], fwd[0]));

                if (angles[0] < 0.0f) angles[0] += 360.0f;
                if (angles[1] < 0.0f) angles[1] += 360.0f;
            }
            angles[2] = 0.0f;
        };

        auto make_vector = [](Vector3 in, Vector3& out) {
            float p = Math::toRad(in[0]),
                y = Math::toRad(in[1]),
                temp = cos(p);

            out[0] = -temp * -cos(y);
            out[1] = sin(y) * temp;
            out[2] = -sin(p);
        };

        vector_angles(dir, ang);
        make_vector(va, aim);
        make_vector(ang, ang);

        return Math::toDeg(acos(aim.dot_product(ang)) / aim.get_length_sqr());
    }

    inline bool can_fire() {
        return g_ql.get_predicted_playerstate()->m_time_to_fire == 0;
    }


public:

    int m_target_index{};
    Entity* m_target{};
    Vector3 m_cur_aim_pos{};

    // ctor.
    Aimbot();

    // think, must be called in CL_WritePacket.
    void think();
};

extern Aimbot g_aimbot;