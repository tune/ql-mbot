#pragma once

#include "ql_math.hpp"

#define	CONTENTS_SOLID			1		// an eye is never valid in a solid
#define	CONTENTS_LAVA			8
#define	CONTENTS_SLIME			16
#define	CONTENTS_WATER			32
#define	CONTENTS_FOG			64
#define	CONTENTS_AREAPORTAL		0x8000
#define	CONTENTS_PLAYERCLIP		0x10000
#define	CONTENTS_MONSTERCLIP	0x20000
#define	CONTENTS_TELEPORTER		0x40000
#define	CONTENTS_JUMPPAD		0x80000
#define CONTENTS_CLUSTERPORTAL	0x100000
#define CONTENTS_DONOTENTER		0x200000
#define	CONTENTS_ORIGIN			0x1000000	
#define	CONTENTS_BODY			0x2000000	
#define	CONTENTS_CORPSE			0x4000000
#define	CONTENTS_DETAIL			0x8000000	
#define	CONTENTS_STRUCTURAL		0x10000000	
#define	CONTENTS_TRANSLUCENT	0x20000000	
#define	CONTENTS_TRIGGER		0x40000000
#define	CONTENTS_NODROP			0x80000000	

#define	SURF_NODAMAGE			0x1		
#define	SURF_SLICK				0x2		
#define	SURF_SKY				0x4		
#define	SURF_LADDER				0x8
#define	SURF_NOIMPACT			0x10	
#define	SURF_NOMARKS			0x20	
#define	SURF_FLESH				0x40	
#define	SURF_NODRAW				0x80	
#define	SURF_HINT				0x100	
#define	SURF_SKIP				0x200	
#define	SURF_NOLIGHTMAP			0x400	
#define	SURF_POINTLIGHT			0x800	
#define	SURF_METALSTEPS			0x1000	
#define	SURF_NOSTEPS			0x2000	
#define	SURF_NONSOLID			0x4000	
#define SURF_LIGHTFILTER		0x8000	
#define	SURF_ALPHASHADOW		0x10000	
#define	SURF_NODLIGHT			0x20000	

#define	MASK_ALL				(-1)
#define	MASK_SOLID				(CONTENTS_SOLID)
#define	MASK_PLAYERSOLID		(CONTENTS_SOLID|CONTENTS_PLAYERCLIP|CONTENTS_BODY)
#define	MASK_DEADSOLID			(CONTENTS_SOLID|CONTENTS_PLAYERCLIP)
#define	MASK_WATER				(CONTENTS_WATER|CONTENTS_LAVA|CONTENTS_SLIME)
#define	MASK_OPAQUE				(CONTENTS_SOLID|CONTENTS_SLIME|CONTENTS_LAVA)
#define	MASK_SHOT				(CONTENTS_SOLID|CONTENTS_BODY|CONTENTS_CORPSE)

enum Weapon_t {
    WP_NONE,
    WP_GAUNTLET,
    WP_MACHINEGUN,
    WP_SHOTGUN,
    WP_GRENADE_LAUNCHER,
    WP_ROCKET_LAUNCHER,
    WP_LIGHTNING,
    WP_RAILGUN,
    WP_PLASMAGUN,
    WP_BFG,
    WP_GRAPPLING_HOOK,
    WP_NAILGUN,
    WP_PROX_LAUNCHER,
    WP_CHAINGUN,
    WP_NUM_WEAPONS
};

enum Gametype_t {
    GT_FFA,
    GT_TOURNAMENT,
    GT_SINGLE_PLAYER,
    GT_TEAM,
    GT_CTF,
    GT_1FCTF
};

class Orientation_t
{
public:
    Vector3		m_origin;
    Vector3		m_axis[3];
};

class Plane_t
{
public:
    Vector3	m_normal;
    float	m_dist;
    BYTE	m_type;
    BYTE	m_signbits;
    char	pad[2];
};

class Trace_t {
public:
    int	allsolid;
    int	startsolid;
    float		fraction;
    Vector3		endpos;
    Plane_t	plane;
    int			surfaceFlags;
    int			contents;
    int			entityNum;  
};

class usercmd_t {
public:
	int				m_servertime;
	int				angles[3];
	int 			buttons;
	byte			weapon;           // weapon 
	signed char	forwardmove, rightmove, upmove;
	char			unk[4];

    static int angle_to_short(float angle) {
        return ((int) (angle) * 65536 / 360) & 65535;
    }

    static float short_to_angle(int angle){
        return ((angle) * (360.0 / 65536));
    }

    void set_angles(const Vector3& in) {
        this->angles[0] = this->angle_to_short(in[0]);
        this->angles[1] = this->angle_to_short(in[1]);
        this->angles[2] = this->angle_to_short(in[2]);
    }

};

class netaddr_t
{
public:
	int32_t m_type; //0x0000
	uint8_t m_ip_1; //0x0004
	uint8_t m_ip_2; //0x0005
	uint8_t m_ip_3; //0x0006
	uint8_t m_ip_4; //0x0007
	char pad_0008[10]; //0x0008
	uint16_t m_port; //0x0012
}; //Size: 0x0014

class ClientConnection
{
public:
	int32_t m_clientnum; //0x0000
	int32_t m_last_packet_sent_time; //0x0004
	int32_t m_last_packet_time; //0x0008
	int32_t m_unk_timestamp; //0x000C
	netaddr_t m_server_addr; //0x0010
	int32_t m_connect_time; //0x0024
}; //Size: 0x0028

class trajectory_t
{
public:
	int32_t m_type; //0x0000
	int32_t m_time; //0x0004
	int32_t m_duration; //0x0008
	Vector3 m_base; //0x000C
	Vector3 m_delta; //0x0018
	int32_t m_unk; //0x0024
}; //Size: 0x0028

class EntityState
{
public:
    int32_t m_number; //0x0000
    int32_t m_type; //0x0004
    int32_t m_flags; //0x0008
    trajectory_t m_traj_pos; //0x000C
    trajectory_t m_traj_ang_pos; //0x0034
    int32_t m_time; //0x005C
    int32_t m_time2; //0x0060
    Vector3 m_origin; //0x0064
    Vector3 m_origin2; //0x0070
    Vector3 m_angles; //0x007C
    Vector3 m_angles2; //0x0088
    char pad_0094[20]; //0x0094
    int32_t m_modelindex; //0x00A8
    char pad_00AC[24]; //0x00AC
    int32_t m_powerups; //0x00C4
    char pad_00C8[8]; //0x00C8
    int32_t m_weapon; //0x00D0
    char pad_00D4[4]; //0x00D4
    int32_t m_legs_anim; //0x00D8
    int32_t m_torso_anim; //0x00DC
    char pad_00E0[12]; //0x00E0

}; //Size: 0x00EC

class Entity
{
public:
	EntityState m_cur_state; //0x0000
	EntityState m_next_state; //0x00EC
	int32_t m_interpolate; //0x01D8
	int32_t m_currentvalid; //0x01DC
	char pad_01E0[216]; //0x01E0
	Vector3 m_lerp_origin; //0x02B4
	Vector3 m_lerp_angles; //0x02C0


    // entity hull_min
    static Vector3 hull_min() {
        static Vector3 hm{-15.f, -15.f, -24.f};
        return hm;
    }
    // entity hull ma x
    static Vector3 hull_max() {
        static Vector3 hm{15.f, 15.f, 32.f};
        return hm;
    }

    // check type to see if player
    inline bool is_player() {
        return (m_cur_state.m_type == 1);
    }

    // check flags to see if alive
    inline bool is_alive() {
        return !(m_cur_state.m_flags & 1);
    }

    // return weapons max effective distance
    float get_weapon_range() {
        switch (m_cur_state.m_weapon) {
        case WP_LIGHTNING:        return 768.f;
        case WP_ROCKET_LAUNCHER:  return 1024.f;
        case WP_GRENADE_LAUNCHER: return 512.f;
        case WP_PLASMAGUN:        return 1024.f;
        case WP_GAUNTLET:         return 32.f; // https://github.com/id-Software/Quake-III-Arena/blob/dbe4ddb10315479fc00086f08e25d968b4b43c49/code/game/g_weapon.c#L81
        case WP_SHOTGUN:          return 512.f;
        default:
            return 8192.f;
        }
    }

    // return speed at which projectiles move
    float get_weapon_speed() {
        switch (m_cur_state.m_weapon) {
        case WP_GRENADE_LAUNCHER: return 700.f;
        case WP_ROCKET_LAUNCHER:  return 1000.f;
        case WP_PLASMAGUN:        return 2000.f;
        case WP_BFG:              return 1800.f;
        default:
            return 0.f;
        }
    }

    // return name of wepaon helf
    std::string get_weapon_name() {
        switch (m_cur_state.m_weapon) {
        case WP_NONE: return "";
        case WP_GAUNTLET:         return STR("gauntlet");
        case WP_MACHINEGUN:       return STR("mg");
        case WP_SHOTGUN:          return STR("shotgun");
        case WP_GRENADE_LAUNCHER: return STR("nade");
        case WP_ROCKET_LAUNCHER:  return STR("rocket");
        case WP_LIGHTNING:        return STR("lg");
        case WP_RAILGUN:          return STR("rail");
        case WP_PLASMAGUN:        return STR("plasma");
        case WP_BFG:              return STR("BFG");
        case WP_GRAPPLING_HOOK:   return STR("hook");
        case WP_NAILGUN:          return STR("nailgun");
        case WP_PROX_LAUNCHER:    return STR("prox");
        case WP_CHAINGUN:         return STR("chaingun");
        case WP_NUM_WEAPONS:      return "";
        default:
            return "";
        }
    }

}; //Size: 0x02D4

class Refdef_t
{
public:
	int32_t m_x; //0x0000
	int32_t m_y; //0x0004
	int32_t m_width; //0x0008
	int32_t m_height; //0x000C
	float m_fov_x; //0x0010
	float m_fov_y; //0x0014
	Vector3 m_view_orig; //0x0018
	Vector3 m_viewaxis_1; //0x0024
	Vector3 m_viewaxis_2; //0x0030
	Vector3 m_viewaxis_3; //0x003C
	int32_t m_time; //0x0048
}; //Size: 0x004C

enum team_t
{
	TEAM_FREE,
	TEAM_RED,
	TEAM_BLUE,
	TEAM_SPECTATOR,

	TEAM_NUM_TEAMS
};

class Clientinfo
{
public:
	int32_t m_valid; //0x0000
	char pad_0004[4]; //0x0004
	char m_name[64]; //0x0008
	char pad_0048[192]; //0x0048
	team_t m_team;
	char pad_0600[496];
	int32_t m_legs_mdl; //0x02FC
	char pad_0300[4]; //0x0300
	int32_t m_torso_mdl; //0x0304
	char pad_0308[4]; //0x0308
	int32_t m_head_mdl; //0x030C
	char pad_0310[1064]; //0x0310

}; //Size: 0x0738

class Cvar
{
public:
    char* m_name; //0x0000
    char* m_string; //0x0004
    char* m_reset_string; //0x0008
    char pad_000C[16]; //0x000C
    int32_t m_flags; //0x001C
    int32_t m_modified; //0x0020
    char pad_0024[4]; //0x0024
    int32_t m_modification_amount; //0x0028
    char pad_002C[8]; //0x002C
    float m_float; //0x0034
    int32_t m_integer; //0x0038
    class Cvar* m_prev; //0x003C
    class Cvar* m_next; //0x0040

    inline void write_string(const std::string& in) {
        static char buf[1024]{};
        
        // set buffer to null
        std::memset(buf, 0, 1024);

        // copy to static buffer
        std::memcpy(buf, in.data(), in.length());

        // replace ptr to our static buffer
        m_string = buf;
    }

};	

class PlayerState
{
public:
	int32_t m_cmd_time; //0x0000
	char pad_0004[16]; //0x0004
	Vector3 m_origin; //0x0014
	Vector3 m_velocity; //0x0020
	int32_t m_time_to_fire; //0x002C
	int32_t m_gravity; //0x0030
	int32_t m_maxspeed; //0x0034
    int32_t m_delta_angles[3];
	int32_t m_groundentity; //0x0044
	char pad_0048[88]; //0x0048
	Vector3 m_viewangles; //0x00A0
	int32_t m_viewheight; //0x00AC
	int32_t m_hurt_events; //0x00B0
	char pad_00B4[8]; //0x00B4
	int32_t m_last_dmg; //0x00BC

}; //Size: 0x00C0


class Snapshot
{
public:
	int32_t m_flags; //0x0000
	int32_t m_ping; //0x0004
	int32_t m_servertime; //0x0008
	char pad_000C[32]; //0x000C
	PlayerState m_playerstate; //0x002C
	int32_t m_num_entities; //0x00EC
}; //Size: 0x00F0

class Cg
{
public:
	int32_t m_clientframe; //0x0000
	int32_t m_clientnum; //0x0004
	char pad_0008[28]; //0x0008
	class Snapshot* m_snapshot; //0x0024
	class Snapshot* m_next_snapshot; //0x0028

}; //Size: 0x002C

