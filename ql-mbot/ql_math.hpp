#pragma once

#include "inc.hpp"

#include <m_vector.h>

using Vector3 = Vector<3>;
using Vector4 = Vector<4>;

using Matrix3x4 = Matrix<float, 3, 4>;
using Matrix3x3 = Matrix<float, 3, 3>;