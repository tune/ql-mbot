#pragma once

#include "ql_client.hpp"

#define Q_COLOR_ESCAPE	'^'
#define Q_IsColorString(p)	( p && *(p) == Q_COLOR_ESCAPE && *((p)+1) && *((p)+1) != Q_COLOR_ESCAPE )
#define ColorIndex(c)	( ( (c) - '0' ) & 7 )


class Render : public IRender {
protected:

	using draw_string_ext_t = void(__cdecl*)(float, float, int, float, float*, const char*, int, int, int);
	using stretch_pic_t = void(__cdecl*)(float x, float y, float w, float h, float s1, float t1, float s2, float t2, int shader);
	using set_color_t = void(__cdecl*)(float* rgba);

	draw_string_ext_t m_draw_string_ext{};
	stretch_pic_t m_stretch_pic{};
	set_color_t m_set_color{};

	int m_whiteshader{};

public:

	inline Render();

	float* make_color(Color col) {
		static float convert[4]{};
		
		convert[0] = col.get(0) / 255.f;
		convert[1] = col.get(1) / 255.f;
		convert[2] = col.get(2) / 255.f;
		convert[3] = col.get(3) / 255.f;

		return convert;
	}

	void convert_to_virtual_resolution(int& x, int& y) {
		float	virtual_x = x * (640.f / g_ql.get_refdef()->m_width),
			virtual_y = y * (480.f / g_ql.get_refdef()->m_height);

		x = (int) virtual_x;
		y = (int) virtual_y;
	}

	virtual void WText(int x, int y, Color col, Font font, Align align, int flags, wchar_t * in){
		std::wstring wstr{in};
		std::string str(wstr.begin(), wstr.end());

		if (!g_ql.get_cg_initialized())
			return;

		// convert px to virtual res
		convert_to_virtual_resolution(x, y);

		// get new CG_DrawStringExt function
		m_draw_string_ext = g_ql.get_drawstringext().as<draw_string_ext_t>();

		char buf[1024]{}, *fuck_u = (char*) str.c_str();
		va_list va_alist;

		va_start(va_alist, fuck_u);
		vsnprintf(buf, sizeof(buf), str.c_str(), va_alist);
		va_end(va_alist);

		m_draw_string_ext(x, y, 3, .18f, make_color(col), buf, 0, 0, 3);
	}
	

	virtual void Gradient(int x, int y, int w, int h, bool horizontal, Color a, Color b);

	virtual void Triangle(int x1, int y1, int x2, int y2, int x3, int y3, Color a);
	
	virtual void Line(int x1, int y1, int x2, int y2, Color col);

	virtual void Filled(int x, int y, int w, int h, Color col);

	virtual void DrawSpace(int& w, int& h);
};

inline Render::Render(){
	m_stretch_pic = Pattern{HASH("quakelive_steam.exe"),STR("55 8B EC 83 3D ? ? ? ? ? 74 78")}.as<stretch_pic_t>();
	m_set_color = Pattern{HASH("quakelive_steam.exe"), STR("55 8B EC 83 3D ? ? ? ? ? 74 5A")}.as<set_color_t>();
	
	auto register_shader = Pattern{HASH("quakelive_steam.exe"),STR("55 8B EC 56 8B 75 08 8B C6 8D"), false, 1}.as<int(__cdecl*)(const char*)>();
	m_whiteshader = register_shader(STR("white"));

    // could use this as well?
    //auto CG_Init_RegisterShaders = Pattern{HASH("quakelive_steam.exe"),STR("68 ? ? ? ? FF 15 ? ? ? ? 68 ? ? ? ? FF 15 ? ? ? ? 68 ? ? ? ? A3")};
    //m_whiteshader = *Pattern{CG_Init_RegisterShaders, 0x200, STR("A3"), false, 2}.at<int*>(0x1);
}

//inline WTEXT

inline void Render::Filled(int x, int y, int w, int h, Color col) {
	m_set_color(make_color(col));
	m_stretch_pic(x, y, w, h, 0.f, 0.f, 1.f, 1.f, m_whiteshader);
	m_set_color(nullptr);
}

inline void Render::DrawSpace(int & w, int & h) {
	w = g_ql.get_refdef()->m_width;
	h = g_ql.get_refdef()->m_height;
}

// incomplete overrides
inline void Render::Gradient(int x, int y, int w, int h, bool horizontal, Color a, Color b)
{
}

inline void Render::Triangle(int x1, int y1, int x2, int y2, int x3, int y3, Color a)
{
}

inline void Render::Line(int x1, int y1, int x2, int y2, Color col)
{
}

extern Render g_render;