#pragma once

#include "ql_includes.hpp"

class Input {
protected:
    Address m_cmds;

public:

    inline Input() : m_cmds{} {
        m_cmds = Pattern{HASH("quakelive_steam.exe"), STR("8D 34 85 ? ? ? ? B9")};

        if (!m_cmds) {
            io::w_printf(STR("-ql couldn't find global CMDs\n"));
            return;
        }

        m_cmds = m_cmds.at(0x3);
        io::w_printf(STR("+ql clientActive_t::usercmd_s[64]\t0x%X\n"), m_cmds.as<uintptr_t>());
    }

    // get usercmd by index (mask assumed)
    usercmd_t* get_usercmd(ptrdiff_t index) {
        return &m_cmds.as<usercmd_t*>()[index & 0x3f];
    }

    // get the current command number
    ptrdiff_t get_current_cmd() {
        return m_cmds.at<ptrdiff_t>(sizeof(usercmd_t[64]));
    }

    // set the current command number
    void set_current_cmd(ptrdiff_t to) {
        Address(m_cmds + sizeof(usercmd_t[64])).set<ptrdiff_t>(to);
    }




};

extern Input g_input;