#pragma once

#include "ql_includes.hpp"
#include "ql_gl32.hpp"

class Quake {
protected:
    using boxtrace_t    = void(__cdecl*)(Trace_t*, Vector3&, Vector3&, float*, float*, int, int, int);
    using modelbounds_t = void(__cdecl*)(int, Vector3&, Vector3&);
    using in_pvs_t      = int(__cdecl*)(Vector3&, Vector3&);
    using cvar_set_t    = Cvar*(__cdecl*)(const char*, const char*, int);
    using find_var_t    = Cvar*(__cdecl*)(const char*);
    using lerp_tag_t    = int(__cdecl*)(Orientation_t*, int, int, int, float, const char*);


    lerp_tag_t    m_lerp_tag{};
    cvar_set_t    m_cvar_set{};
    boxtrace_t    m_boxtrace{};
    modelbounds_t m_modelbounds{};
    in_pvs_t      m_in_pvs{};
    find_var_t    m_find_var{};

    Address m_predps,
        m_clientinfo,
        m_drawstringext,
        m_cl_viewangles,
        m_cg_viewangles,
        m_gametype,
        m_refexport,
        m_entities,
        m_weapons,
        m_items,
        m_clc,
        m_cgs,
        m_cl,
        m_cg;

    bool	m_initialized{},
            m_scanned{};

    Refdef_t *m_refdef{}, 
             *m_refdef2{};



public:
    Entity     *m_local;
    Vector3     m_viewangles, m_head_pos;
    usercmd_t*  m_cmd, *m_old, *m_next;
    Clientinfo* m_local_info;
    int         m_clientnum;

    // ctor
    inline Quake();

    // thunk
    void think();

    // translate 3d coordinate to 2d pix coordinates.
    bool to_screen(Vector3& v, Vector3& out);

    // check if cgamex86 module has loaded
    inline bool probe_for_cg();

    // get centity_t by index
    template<class _T = Entity*>
    inline _T get_entity(ptrdiff_t index);

    /* misc sets */

    // set refdef in RE_RenderScene
    inline void set_refdef(Refdef_t* right) {
        m_refdef = right;
    }

    /* misc gets */

    // get refdef_t* parameter from RE_Renderscene
    inline Refdef_t* get_refdef() {
        return m_refdef;
    }

    // get cg_weapons
    inline Address get_weapons() {
        return m_weapons;
    }

    // get cg_items
    inline Address get_items() {
        return m_items;
    }

    // retrieve clientconnection static (has clientnum and maybe some other useful stuff)
    inline ClientConnection* get_clc() {
        return m_clc.as<ClientConnection*>();
    }

    // get refexport_t* (containing essential methods)
    Address get_refexport() {
        return m_refexport;
    }

    // get CG_DrawStringExt
    inline Address get_drawstringext() {
        return m_drawstringext;
    }

    // get Clientinfo* for entity at given index
    inline Clientinfo* get_info(ptrdiff_t index) {
        if (!this->get_cg_initialized())
            return nullptr;

        return &m_clientinfo.as<Clientinfo*>()[index];
    }

    // cgamex86 is loaded on the fly upon game init.. to make other features work while this is unavailable, we use a helper method
    bool get_cg_initialized() {
        static auto probe = Timer<std::chrono::milliseconds>();

        if (probe.diff() < 10 && !m_initialized) {

        }
        else {
            m_initialized = probe_for_cg();

            probe.reset();
        }

        return m_initialized;
    }

    // call R_ModelBounds to retrieve mins/maxs for model handle
    void get_model_bounds(int model_handle, Vector3& mins, Vector3& maxs) {
        return m_modelbounds(model_handle, mins, maxs);
    }

    // this is probably useless
    inline bool in_pvs(Vector3& p1, Vector3& p2) {
        return !!m_in_pvs(p1, p2);
    }

    // call Cvar_FindVar and retrieve cvar_t*
    Cvar* get_cvar(const std::string& search) {
        return m_find_var(search.c_str());
    }

    // get cg_t
    Cg* get_cg() {
        return m_cg.as<Cg*>();
    }

    // get viewangles
    Vector3 get_viewangles() {
        return get_cg_initialized() ? *m_cg_viewangles.as<Vector3*>() : *m_cl_viewangles.as<Vector3*>();
    }

    // set viewangles
    void set_viewangles(const Vector3& to) {
        //get_cg_initialized() ? *m_cg_viewangles.as<Vector3*>() = to : *m_cl_viewangles.as<Vector3*>() = to;
        *m_cl_viewangles.as<Vector3*>() = to;
    }

    // call CM_BoxTrace and check fraction
    bool is_visible(Vector3& start, Vector3& end, Entity* ent = nullptr) {
        Trace_t tr;
        m_boxtrace(&tr, start, end, nullptr, nullptr, 0, MASK_SHOT, 0);

        return tr.fraction == 1.f;
    }

    // retrieve cl.predictedPlayerState
    PlayerState* get_predicted_playerstate() {
        return m_predps.as<PlayerState*>();
    }

    // invoke Cvar_Set2 with force 
    void set_cvar(const std::string& name, const std::string& value) {
        m_cvar_set(name.c_str(), value.c_str(), 1/*force*/);
    }

    // check that snapshot is valid
    bool valid_snapshot() {
        if (!get_cg_initialized())
            return false;
        if (!Address::safe(get_cg()))
            return false;
        if (!Address::safe(get_cg()->m_snapshot))
            return false;

        return true;
    }

    // return current game type
    inline Gametype_t get_gametype() {
        return *m_gametype.as<Gametype_t*>();
    }

    // get tag position
    Vector3 get_tag(int model, const std::string& tag) {
        auto rotate = [](Vector3 in, Vector3* axis)->Vector3 {
            return Vector3{
                in.dot_product(axis[0]),
                in.dot_product(axis[1]),
                in.dot_product(axis[2])
            };
        };

        Vector3       out{};
        Orientation_t o{};

        m_lerp_tag(&o, model, 0x7FFFFFFF, 0x7FFFFFFF, 1.f, tag.c_str());

        return rotate(o.m_origin, o.m_axis);
    }

    // get entity head position
    inline Vector3 get_head_for_ent(int index) {
        auto ent = get_entity(index);
        auto ci = get_info(index);

        auto legs_tag = get_tag(ci->m_legs_mdl, STR("tag_torso"));
        auto head_tag = get_tag(ci->m_torso_mdl, STR("tag_head"));

        return ent->m_lerp_origin + head_tag + legs_tag;
    }

    // get head height
    float get_viewheight_for_ent(int index) {
        auto ent = get_entity(index);
        auto ci = get_info(index);

        auto legs_tag = get_tag(ci->m_legs_mdl, STR("tag_torso"));
        auto head_tag = get_tag(ci->m_torso_mdl, STR("tag_head"));

        return head_tag[2] + legs_tag[2];
    }
};

Quake::Quake() : m_entities{} {

    auto ql = Module::base();

    m_modelbounds   = Pattern{ql, ql.get_img_size(), STR("55 8B EC 8B 45 08 83 F8 01 7C 11 3B 05 ? ? ? ? 7D 09 8B 04 85 ? ? ? ? EB 05")}.as<modelbounds_t>();
    m_in_pvs        = Pattern{ql, ql.get_img_size(), STR("55 8B EC 56 8B 75 08 57 E8")}.as<in_pvs_t>();
    m_find_var      = Pattern{ql, ql.get_img_size(), STR("55 8B EC 56 57 8B 7D 08 8B C7 E8")}.as<find_var_t>();
    m_cl_viewangles = Pattern{ql, ql.get_img_size(), STR("D9 1D ? ? ? ? E8 ? ? ? ? D9 45 FC")}.at(0x2);
    m_boxtrace      = Pattern{ql, ql.get_img_size(), STR("55 8B EC 8B 45 24 8B 4D 20 8B 55 1C 6A 00")}.as<boxtrace_t>();
    m_refexport     = Pattern{ql, ql.get_img_size(), STR("A1 ? ? ? ? 83 C4 0C 85 C0 74 18")};
    m_cvar_set      = Pattern{ql, ql.get_img_size(), STR("55 8B EC 51 53 8B 5D 0C 56 57 8B 7D 08")}.as<cvar_set_t>();
    m_lerp_tag      = Pattern{ql, ql.get_img_size(), STR("55 8B EC 8B 45 0C 83 F8 01")}.as<lerp_tag_t>();

    if (!m_refexport) {
        io::w_printf(STR("-ql couldn't find refexport_t\n"));
        return;
    }
    else {
        m_refexport = m_refexport.at(0x1);
        io::w_printf(STR("+ql found refexport_t::re\t\t0x%X\n"), m_refexport);
    }
    m_clc = Pattern{ql, ql.get_img_size(), STR("8B 0D ? ? ? ? 8B 15 ? ? ? ? A1 ? ? ? ? 51 8B 0D ? ? ? ? 52 8B 11")};
    if (!m_clc) {
        io::w_printf(STR("-ql couldn't find clc\n"));
        return;
    }
    else {
        m_clc = m_clc.at(0x2);
        io::w_printf(STR("+ql found clientConnection_t::clc\t0x%X\n"), m_clc);
    }

    m_refdef2 = Pattern{ql, ql.get_img_size(), STR("A1 ? ? ? ? D9 05 ? ? ? ? 8B 0D")}.at<Refdef_t*>(0x1);
    io::w_printf(STR("+ql found refdef_t::refdef\t\t0x%X\n"), m_refdef2);

}
template<class _T = Entity*>
inline _T Quake::get_entity(ptrdiff_t index) {
    if (!this->get_cg_initialized())
        return _T{};

    return &m_entities.as<Entity*>()[index];
}

inline void Quake::think() {
    auto clientnum = get_clc()->m_clientnum;

    if (Address::safe(get_predicted_playerstate())) {
        m_head_pos = get_predicted_playerstate()->m_origin + Vector3{0.f, 0.f,get_predicted_playerstate()->m_viewheight};
    }
    else if (valid_snapshot()) {
        auto ps = get_cg()->m_snapshot->m_playerstate;

        m_head_pos = ps.m_origin + Vector3{0.f, 0.f,ps.m_viewheight};
    }

    m_clientnum = clientnum;
    m_local = get_entity(clientnum);
    m_local_info = get_info(clientnum);

    m_viewangles = get_viewangles();
}

inline bool Quake::probe_for_cg() {
    static auto last_cg = Module{};
    auto cg = Module{};

    // todo; find solution
    //
    // ghetto hack to fix my glaring peb issue, 
    // where if the module amount loaded is the 
    // same upon load/unload of a module.
    // (e.g. 'cgamex86.dll')

    bool got_mod{true};
    auto cg_addr{(uintptr_t) GetModuleHandleA(STR("cgamex86.dll"))};
    auto offsets_dead = (!m_items && !m_weapons && !m_entities && !m_cg && !m_cgs);

    // request peb update and get module
    if (cg_addr != last_cg.as<uintptr_t>()) {
        // update peb
        Peb::inst(true);

        // get module
        got_mod = Peb::inst()->get_module(HASH("cgamex86.dll"), cg);

        // setup to scan
        m_scanned = false;

        // fixup by setting last mod to newly updated.
        last_cg = cg;
    }

    auto update_offsets = [ & ]()-> bool {
        auto cg_init = Pattern(cg, cg.get_img_size(), STR("8B 54 24 08 53"));

        if (!cg_init) {
            m_scanned = false;
            return false;
        }
        io::w_printf(STR("--------------- game initializing ---------------\n"));

        m_cgs = Pattern{cg_init, 0x200, STR("BF")}.at(0x1);
        io::w_printf(STR("+ql cgs\t\t\t\t\t0x%X\n"), m_cgs);

        m_cg = Pattern{cg_init, 0x200, STR("BF"), false, 1}.at(0x1);
        io::w_printf(STR("+ql cg\t\t\t\t\t0x%X\n"), m_cg);

        m_entities = Pattern{cg_init, 0x200, STR("BF"), false, 2}.at(0x1);
        io::w_printf(STR("+ql cg_entities\t\t\t\t0x%X\n"), m_entities);

        m_weapons = Pattern{cg_init, 0x200, STR("BF"), false, 3}.at(0x1);
        io::w_printf(STR("+ql cg_weapons\t\t\t\t0x%X\n"), m_weapons);

        m_items = Pattern{cg_init, 0x200, STR("BF"), false, 4}.at(0x1);
        io::w_printf(STR("+ql cg_items\t\t\t\t0x%X\n"), m_items);

        m_clientinfo    = Pattern{cg, cg.get_img_size(), STR("81 C6 ? ? ? ? 33 FF")}.at(0x2);
        m_drawstringext = Pattern{cg, cg.get_img_size(), STR("83 EC 24 A1 ? ? ? ? 33 C4 89 44 24 20 55")};
        m_predps        = Pattern{cg, cg.get_img_size(), STR("BF ? ? ? ? 89 15")}.at(0x1);
        m_cg_viewangles = Pattern{cg, cg.get_img_size(), STR("D8 05 ? ? ? ? D9 1D ? ? ? ? D9 5C 24 0C")}.at(0x2);
        m_gametype      = Pattern{cg, cg.get_img_size(), STR("A1 ? ? ? ? 83 C4 10 83 F8 03")}.at(0x1);

        m_scanned = (m_items
                     && m_weapons
                     && m_entities
                     && m_cg
                     && m_cgs
                     && m_clientinfo
                     && m_drawstringext
                     && m_predps
                     && m_cg_viewangles
                     && m_gametype);

        return m_scanned;
    };

    if (!got_mod) {
        m_scanned = false;
        return false;
    }

    if (m_scanned && offsets_dead)
        m_scanned = false;

    if (!m_scanned)
        return update_offsets();
    else
        return m_scanned;
}

inline bool Quake::to_screen(Vector3& v, Vector3& out) {
    auto def = get_refdef();

    Vector3 trans;
    float	xc, yc,
        px, py,
        z;

    px = std::tan(def->m_fov_x * Math::pi / 360.f);
    py = std::tan(def->m_fov_y * Math::pi / 360.f);

    trans = def->m_view_orig.InvSub(v);

    xc = def->m_width / 2.f;
    yc = def->m_height / 2.f;
    z = trans.dot_product(def->m_viewaxis_1);

    if (z <= 0.001f)
        return false;

    out[0] = xc - trans.dot_product(def->m_viewaxis_2) * xc / (z * px);
    out[1] = yc - trans.dot_product(def->m_viewaxis_3) * yc / (z * py);

    return true;
}

extern Quake g_ql;