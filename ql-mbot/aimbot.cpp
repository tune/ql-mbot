#include "aimbot.hpp"

Aimbot::Aimbot() :
    m_cur_aim_pos{},
    m_target{},
    m_target_index{}
{
}

bool Aimbot::is_valid_target(Entity * ent, Clientinfo* info)
{
    if (!ent
        || ent == g_ql.m_local
        || !ent->m_currentvalid
        || !ent->is_player()
        || !ent->is_alive()
        || !info->m_valid
        || g_ql.get_gametype() != GT_FFA && (info->m_team == g_ql.m_local_info->m_team) )
        return false;

    return true;
}

bool Aimbot::get_aimpoint(Entity * ent, Clientinfo* info, Vector3 & out_pos) {
    int        any_visible = -1;
    Vector3    mins = Entity::hull_min(),
               maxs = Entity::hull_max();
    Matrix3x4  mat{ent->m_lerp_angles.matrix(ent->m_lerp_origin)};
    float      weapon_speed = g_ql.m_local->get_weapon_speed();


    maxs[2] = g_ql.get_viewheight_for_ent(ent->m_cur_state.m_number);

    std::array<Vector3, 14>points_to_check{
        Vector3{mins[0] * .75, mins[1] * .75, mins[2] * .75},
        Vector3{mins[0] * .75, mins[1] * .75, maxs[2] * .75},
        Vector3{mins[0] * .75, maxs[1] * .75, mins[2] * .75},
        Vector3{mins[0] * .75, maxs[1] * .75, maxs[2] * .75},
        Vector3{maxs[0] * .75, mins[1] * .75, mins[2] * .75},
        Vector3{maxs[0] * .75, mins[1] * .75, maxs[2] * .75},
        Vector3{maxs[0] * .75, maxs[1] * .75, mins[2] * .75},
        Vector3{maxs[0] * .75, maxs[1] * .75, maxs[2] * .75},
        Vector3{0.f, 0.f, maxs[2] * -.25},
        Vector3{0.f, 0.f, maxs[2] * -.50},
        Vector3{0.f, 0.f, maxs[2] * -.75},
        Vector3{0.f, 0.f, maxs[2] * .25},
        Vector3{0.f, 0.f, maxs[2] * .50},
        Vector3{0.f, 0.f, maxs[2] * .75}
    };

    for (auto i{points_to_check.size() - 1}; i; i--) {
        // aim at the ground with rockets
        if (g_ql.m_local->m_cur_state.m_weapon == WP_ROCKET_LAUNCHER)
            points_to_check[i] = ent->m_lerp_origin;
        else
            points_to_check[i] = points_to_check[i].transform(mat);


        // add projectile compensation
        if (weapon_speed) {
            auto snap = g_ql.get_cg()->m_snapshot;
            auto next_snap = g_ql.get_cg()->m_next_snapshot;

            if (snap && next_snap) {
                auto state = ent->m_cur_state;
                auto next_state = ent->m_next_state;

                Vector3 target_velocity = (next_state.m_traj_pos.m_base - state.m_traj_pos.m_base) * (1000.f / (next_snap->m_servertime - snap->m_servertime));
                Vector3 target_delta = points_to_check[i] - g_ql.m_head_pos;

                float time = target_delta.get_length() / (weapon_speed - target_velocity.get_length());

                points_to_check[i] += (target_velocity * time);
            }
        }

        if (g_ql.is_visible(g_ql.m_head_pos, points_to_check[i], ent)) {
            any_visible = i;
            out_pos = points_to_check[i];
            break;
        }
    }

    return (any_visible != -1);
}

bool Aimbot::get_aim_angles(Vector3 & out_angles)
{
    int             target_index{-1};
    Entity          *ent, *target{nullptr};
    float           dist, max_dist {g_ql.m_local->get_weapon_range()};
    Vector3         tmp_pos, best_pos, aim_angles;
    Clientinfo*     info;

    dist = 0.f;

    // reset member vars.
    m_target_index = -1;
    m_target = nullptr;
    m_cur_aim_pos = Vector3{};

    // iterate players.
    for (int i{0}; i <= 64; ++i) {
        ent = g_ql.get_entity(i);
        info = g_ql.get_info(i);

        // check if ent is valid.
        if (!is_valid_target(ent, info))
            continue;

        // get hitbox.
        if (!get_aimpoint(ent, info, tmp_pos))
            continue;

        const float fov = 180.f;

        // fov check.
        if (fov != 0.f) {
            if (get_fov(g_ql.m_viewangles, g_ql.m_head_pos, tmp_pos) > fov)
                continue;
        }

        // dist check.
        dist = g_ql.m_head_pos.get_distance(tmp_pos);
        if (dist > max_dist)
            continue;

        // todo; next shot.

        // set some vars.
        target_index = i;
        target = ent;
        max_dist = dist;
        best_pos = tmp_pos;
    }

    // check if target data is valid.
    if (target_index != -1 && target != nullptr) {     
        // calculate aim angles.
        aim_angles = (best_pos - g_ql.m_head_pos).to_se_angle();

        // save target data / set out angles.
        m_target_index = target_index;
        m_target = target;
        m_cur_aim_pos = best_pos;
        out_angles = aim_angles;

        return true;
    }

    return false;
}

void Aimbot::name_spam()
{
    // self explanatory shit code lol
    static Cvar* name_cvar = g_ql.get_cvar(STR("name"));
    static Timer<std::chrono::milliseconds> timer{};

    if (timer.diff() > 5) {

        static std::array<std::string, 6> fag_shit{
          STR("Stevia is the cure for the human body."),
          STR("perfekcyjnym hookiem za milion dolar�w"),
          STR("jezdze po miescie z louis vitton perfekcyjny hack"),
          STR("Fuck Hackers & Cheaters!"),
          STR("Wh'o watching this is December "),
          STR("in der hood")
        };

        g_ql.set_cvar(STR("name"), fag_shit[rand() % 6]);

        timer.reset();
    }
}

void Aimbot::name_steal() {
    static Timer<std::chrono::milliseconds> timer{};
    static std::vector<std::string> names{};

    // must wait 1 second per client command
    // https://github.com/id-Software/Quake-III-Arena/blob/dbe4ddb10315479fc00086f08e25d968b4b43c49/code/server/sv_client.c#L1288-L1289
    if (timer.diff() > 1000) {
        // stuff vector with names
        for (int i{}; i < 64; ++i) {
            if (i == g_ql.m_clientnum)
                continue;
            
            auto* ci = g_ql.get_info(i);
            
            if (!Address::safe(ci))
                continue;
            if (!ci->m_valid)
                continue;

            names.push_back(ci->m_name);
        }

        // check that names aren't empty to prevent div0
        if (names.empty())
            return;

        // set name to random name
        g_ql.set_cvar(STR("name"), names[rand() % names.size()] + "    \0");
        
        // clear all names
        names.clear();
        
        // reset timer
        timer.reset();
    }
}

void Aimbot::fix_move(Vector3 aim_angles) {
    auto angle_normalize = [](float angle) {
        return (360.0 / 65536) * ((int) (angle * (65536 / 360.0)) & 65535);
    };

    float move_angle, delta_angle,
        dest_angle, fwd_ratio,
        right_ratio;

    int fwd = g_ql.m_old->forwardmove,
        right = g_ql.m_old->rightmove;

    auto ps = g_ql.get_predicted_playerstate();

    if (fwd != 0 || right != 0) {
        move_angle  = angle_normalize(Math::toDeg(std::atan2(-right / 127.0, fwd / 127.0)));
        delta_angle = angle_normalize(aim_angles[1] - (ps->m_viewangles[1] - usercmd_t::short_to_angle(ps->m_delta_angles[1])));
        dest_angle  = angle_normalize(move_angle - delta_angle);

        fwd_ratio   = std::cos(Math::toRad(dest_angle));
        right_ratio = -std::sin(Math::toRad(dest_angle));

        if (std::abs(fwd_ratio) < std::abs(right_ratio)) {
            fwd_ratio *= 1.0 / std::abs(right_ratio);
            right_ratio = right_ratio > .0 ? 1.0 : -1.0;
        }
        else if (std::abs(fwd_ratio) > std::abs(right_ratio)) {
            right_ratio *= 1.0 / std::abs(fwd_ratio);
            fwd_ratio = fwd_ratio > .0 ? 1.0 : -1.0;
        }
        else {
            fwd_ratio = 1.0;
            right_ratio = 1.0;
        }

        fwd   = fwd_ratio * 127;
        right = right_ratio * 127;

        g_ql.m_old->forwardmove = fwd;
        g_ql.m_old->rightmove = right;
    }
}

void Aimbot::think()
{
    Vector3 aim_angles{};

    const bool steal_names{true},
        spam_names{false},
        silent_aim{true};

    if (!g_ql.valid_snapshot())
        return;

    if (!Address::safe(g_ql.m_local) ||
        !Address::safe(g_ql.get_predicted_playerstate()))
        return;

    if (!get_aim_angles(aim_angles))
        return;   

    if (!g_ql.m_local_info->m_valid ||
        !g_ql.m_local->is_alive())
        return;

    if (steal_names)
        name_steal();

    if (spam_names)
        name_spam();

    // subtract predicted delta
    aim_angles[0] -= usercmd_t::short_to_angle(g_ql.get_predicted_playerstate()->m_delta_angles[0]);
    aim_angles[1] -= usercmd_t::short_to_angle(g_ql.get_predicted_playerstate()->m_delta_angles[1]);

    // set angles
    g_ql.m_cmd->angles[0] = usercmd_t::angle_to_short(aim_angles[0]);
    g_ql.m_cmd->angles[1] = usercmd_t::angle_to_short(aim_angles[1]);

    if (can_fire())
        g_ql.m_cmd->buttons |= 0x1/*ATTACK*/;

    // non silent aim
    if(!silent_aim)
        g_ql.set_viewangles(aim_angles);

    // move fix (no longer needed)
    //fix_move(aim_angles);
}

