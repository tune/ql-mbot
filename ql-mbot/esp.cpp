#include "esp.hpp"

ESP::ESP() {

}

void ESP::draw_name(Clientinfo* ci, int x, int y, int w, int h) {
    g_render.Text(x + (w * .5f), y - 9, colors::white, 3, IRender::center, 3, STR("%s"), ci->m_name);
}

void ESP::draw_box(team_t team, bool visible, int x, int y, int w, int h) {
    g_render.Box(x, y, w, h, visible ? (team == team_t::TEAM_BLUE ? colors::blue : colors::red) : (team == team_t::TEAM_BLUE ? colors::lightBlue : colors::lightRed));
}

void ESP::draw_weapon(Entity* ent, int x, int y, int w, int h) {
    g_render.Text(x + (w * .5f), y + h + 2, colors::white, 3, IRender::center, 3, STR("%s"), ent->get_weapon_name().c_str());
}

bool ESP::make_box(Entity* ent, Clientinfo*ci, int& x, int&y, int&w, int&h) {

    int tempx,
        tempy,
        tempw,
        temph;

    Vector3
        orig = ent->m_lerp_origin,
        mins = Entity::hull_min(),
        maxs = Entity::hull_max();

    maxs[2] = g_ql.get_viewheight_for_ent(ent->m_cur_state.m_number);

    std::array<Vector3, 8> screen, corners{
        Vector3{mins[0], mins[1], mins[2]},
        Vector3{mins[0], mins[1], maxs[2]},
        Vector3{mins[0], maxs[1], mins[2]},
        Vector3{mins[0], maxs[1], maxs[2]},
        Vector3{maxs[0], mins[1], mins[2]},
        Vector3{maxs[0], mins[1], maxs[2]},
        Vector3{maxs[0], maxs[1], mins[2]},
        Vector3{maxs[0], maxs[1], maxs[2]}
    };

    g_render.DrawSpace(tempw, temph);
    tempx = tempw * 2;
    tempy = temph * 2;
    tempw = temph = -9999;

    Matrix3x4 mat = ent->m_lerp_angles.matrix(ent->m_lerp_origin);
    for (int i{}; i < 8; i++) {
        corners[i] = corners[i].transform(mat);

        if (!g_ql.to_screen(corners[i], screen[i]))
            continue;

        tempx = min(tempx, screen[i][0]);
        tempy = min(tempy, screen[i][1]);
        tempw = max(tempw, screen[i][0]);
        temph = max(temph, screen[i][1]);
    }

    tempw -= tempx;
    temph -= tempy;

    x = tempx;
    y = tempy;
    w = tempw;
    h = temph;

    return true;
}

void ESP::draw() {
    int x, y, w, h;

    auto local_client = g_ql.get_clc()->m_clientnum;

    auto local = g_ql.get_entity(local_client);
    auto local_info = g_ql.get_info(local_client);

    for (int i{0}; i < 64; ++i) {
        auto ent = g_ql.get_entity(i);
        auto info = g_ql.get_info(i);

        if (!ent || !info)
            continue;

        if (!Address::safe(ent) || !Address::safe(info))
            continue;

        if (!info->m_valid || !ent->m_currentvalid)
            continue;

        if (ent->m_cur_state.m_number == local_client)
            continue;

        if (!ent->is_player() || !ent->is_alive())
            continue;

        if (ent->m_lerp_origin.empty())
            continue;

        if (!make_box(ent, info, x, y, w, h))
            continue;

        auto vis = g_ql.is_visible(g_ql.m_head_pos, ent->m_lerp_origin);

        draw_box(info->m_team, vis, x, y, w, h);
        draw_name(info, x, y, w, h);
        draw_weapon(ent, x, y, w, h);
    }
}
