#include "ql_input.hpp"
#include "ql_client.hpp"
#include "ql_render.hpp"

#include "esp.hpp"
#include "aimbot.hpp"

Input  g_input{};
Quake  g_ql{};
gl32   g_gl32{};
Render g_render{};

ESP    g_esp{};
Aimbot g_aimbot{};

namespace hooks_fuck_u {

    Detour<usercmd_t*(__cdecl*)(usercmd_t*)> m_create_cmd_dtr{};
    Detour<void(__cdecl*)(void)> m_create_new_cmds_dtr{};
    Detour<void(__cdecl*)(void)> m_writepacket_dtr{};
    Detour<int(__cdecl*)(void)> m_readytosend_dtr{};

    void(*m_endframe_orig)(int*, int*) {};
    void(*m_renderscene_orig)(Refdef_t*) {};

    int __cdecl CL_ReadyToSendPacket() {
        static int choked{};
        int send_packet = m_readytosend_dtr.get()();

        const bool fake_lag = false;
        const bool speed_hack = false;

        if (speed_hack) {
            // call stack:
            // return to CL_SendCmd
            // return to Com_Frame(?)   
            auto stack = Stack();

            if (GetAsyncKeyState('F')) {
                static int factor{5};
                const int reset{5};

                auto return_address = Address{stack.next().as() + 0x4};

                if (factor--) {
                    return_address.set(return_address.get(1) - 0x5);
                    return false;
                }
                else
                    factor = reset;
            }
        }

        if (fake_lag) {
            send_packet = 0;
            choked++;

            if (!send_packet) {
                if (choked < 32)
                    choked++;
                else {
                    send_packet = 1;
                    choked = 0;
                }
            }
            else
                choked = 0;
        }

        return send_packet;
    }

    void __cdecl RE_EndFrame(int* fms, int* bms) {

        if (!Address::safe(g_ql.get_refdef()))
            return m_endframe_orig(fms, bms);

        if (!g_ql.valid_snapshot())
            return m_endframe_orig(fms, bms);

        if (!Address::safe(g_ql.get_cg()))
            return m_endframe_orig(fms, bms);

        // watermark
        g_render.Text(g_ql.get_refdef()->m_width * .1f, g_ql.get_refdef()->m_height * .11f, colors::white, 0, 0, 0, "micro %s", __DATE__);

        // client think
        g_ql.think();

        // esp think
        g_esp.draw();

        return m_endframe_orig(fms, bms);
    }

    void __cdecl RE_RenderScene(Refdef_t* refdef) {
        g_ql.set_refdef(refdef);

        m_renderscene_orig(refdef);
    }

    void __cdecl CL_CreateNewCommands() {
        m_create_new_cmds_dtr.get()();

        // check for cg init
        if (!g_ql.get_cg_initialized())
            return;

        auto cur_cmd = g_input.get_current_cmd();

        // store current and previous command
        g_ql.m_old = g_input.get_usercmd(cur_cmd - 1);
        g_ql.m_cmd = g_input.get_usercmd(cur_cmd);
        g_ql.m_next = g_input.get_usercmd(cur_cmd + 1);

        // make space for the new old cmd and write the old cmd to that spa
        *g_ql.m_next = *g_ql.m_cmd;
        *g_ql.m_cmd = *g_ql.m_old;

        // increment current server time
        g_ql.m_cmd->m_servertime++;

        // increment current command
        g_input.set_current_cmd(cur_cmd + 1);

        // run aimbot think
        g_aimbot.think();

        return;
    }

    // deprecated - use CL_CreateNewCommands
    void __cdecl CL_WritePacket() {        
        if (!g_ql.get_cg_initialized())
            return m_writepacket_dtr.get() ();

        return m_writepacket_dtr.get()();
    }

    usercmd_t* __cdecl CL_CreateCmd(usercmd_t* cmd) {
        auto* ret = m_create_cmd_dtr.get()(cmd);

        return ret;
    }
}

unsigned long __stdcall init(void* lp) {
    auto con = Console{STR("qlmbot")};
    io::w_printf(STR("---------------       MICRO       ---------------\n"));

    g_input  = Input();
    g_ql     = Quake();
    g_gl32   = gl32();
    g_render = Render();

    g_esp    = ESP();
    g_aimbot = Aimbot();

    auto create_cmd      = Pattern{HASH("quakelive_steam.exe"), STR("55 8B EC 83 EC 0C D9 05")};
    auto ready_to_send   = Pattern{HASH("quakelive_steam.exe"), STR("83 3D ? ? ? ? ? 56 57 0F 85")};
    auto create_new_cmds = Pattern{HASH("quakelive_steam.exe"), STR("55 8B EC 83 EC 1C 83 3D ? ? ? ? ? 7C 63")};
    auto writepacket     = Pattern{HASH("quakelive_steam.exe"), STR("55 8B EC B8 ? ? ? ? E8 ? ? ? ? A1 ? ? ? ? 33 C5 89 45 FC 83 3D ? ? ? ? ? 0F 85")};

    if (writepacket) {
        hooks_fuck_u::m_writepacket_dtr.detour(writepacket, hooks_fuck_u::CL_WritePacket);
        io::w_printf(STR("+ql hooked CL_WritePacket\t\t0x%X\n"), writepacket.as<uintptr_t>());
    }
    else
        io::w_printf(STR("-ql couldn't find CL_WritePacket\n"));
    
    if (create_cmd) {
        hooks_fuck_u::m_create_cmd_dtr.detour(create_cmd, hooks_fuck_u::CL_CreateCmd);
        io::w_printf(STR("+ql hooked CL_CreateCmd\t\t\t0x%X\n"), create_cmd.as<uintptr_t>());
    }
    else 
        io::w_printf(STR("-ql couldn't find CL_CreateCmd\n"));   

    if (ready_to_send) {
        hooks_fuck_u::m_readytosend_dtr.detour(ready_to_send, hooks_fuck_u::CL_ReadyToSendPacket);
        io::w_printf(STR("+ql hooked CL_ReadyToSendPacket\t\t0x%X\n"), ready_to_send.as<uintptr_t>());
    }
    else 
        io::w_printf(STR("-ql couldn't find CL_ReadyToSendPacket\n"));
    

    if (create_new_cmds) {
        hooks_fuck_u::m_create_new_cmds_dtr.detour(create_new_cmds, hooks_fuck_u::CL_CreateNewCommands);
        io::w_printf(STR("+ql hooked CL_CreateNewCommands\t\t0x%X\n"), create_new_cmds.as<uintptr_t>());
    }
    else {

        io::w_printf(STR("-ql couldn't find CL_CreateNewCommands\n"));
    }


    // strip name flags
    auto name_cvar = g_ql.get_cvar(STR("name"));
    name_cvar->m_flags &= ~64/*CVAR_ROM*/;

    // strip cheat flags
    auto sv_cheats = g_ql.get_cvar(STR("sv_cheats"));
    sv_cheats->m_flags &= ~64/*CVAR_ROM*/;
    sv_cheats->m_flags &= ~512/*CVAR_CHEAT*/;

    hooks_fuck_u::m_endframe_orig = ((void(*)(int*, int*))g_ql.get_refexport().as<uintptr_t*>()[22]);
    g_ql.get_refexport().as<uintptr_t*>()[22] = (uintptr_t) std::addressof(hooks_fuck_u::RE_EndFrame);
    io::w_printf(STR("+ql hooked RE_EndFrame\t\t\t0x%X\n"), hooks_fuck_u::m_endframe_orig);

    hooks_fuck_u::m_renderscene_orig = ((void(*)(Refdef_t*))g_ql.get_refexport().as<uintptr_t*>()[15]);
    g_ql.get_refexport().as<uintptr_t*>()[15] = (uintptr_t) std::addressof(hooks_fuck_u::RE_RenderScene);
    io::w_printf(STR("+ql hooked RE_RenderScene\t\t0x%X\n"), hooks_fuck_u::m_renderscene_orig);

    return std::getchar();
}

int __stdcall DllMain(HMODULE inst, int reason, uintptr_t) {
    if (reason == 1) {
        srand(time(0)); // im a fag
        DisableThreadLibraryCalls(inst);
        CreateThread(0, 0, init, inst, 0, 0);
    }

    return{1};
}